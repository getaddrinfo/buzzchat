[[_TOC_]]

# BuzzChat Example

This application shows how to use the
[websocket](https://github.com/gorilla/websocket) package to implement a simple
web chat application. This code has been modified from one of the examples. 

## **IMPORTANT STUFF**
* **[Original License](https://github.com/gorilla/websocket/blob/master/LICENSE)**
* **[Original Unmodified Code](https://github.com/gorilla/websocket/tree/master/examples/chat)**
* **[Authors deserving much thanks!](https://github.com/gorilla/websocket/blob/master/AUTHORS)**

# Purpose

Besides haveing a better example than an echo server for BuzzCrate, use of a chat system for confrences would be useful
as well as 2020-2021 virtual meetings. The goal for this project is to have a deployable solution even more 
deployable than previous solutions with the use of runners anyone can use localy. Additionally, websockets are SUPER
COOL and I'm so glad to have a project to introduce them to people. 

Please checkout the original source code as the Gorilla Webosockets project is very mature and extremely helpful in 
general. 


# Installation Local

If you're new to go... set up go path. Once set up, first time builds in the checked out folder are this simple:

```bash
$ go get
$ go build
$ ./buzzchat
```

If on a local system, to [https://localhost:8080](https://localhost:8080). To change name in the window, simply 
use `/nick {your_new_handle}` and voila, you're ready to rock. 

To build changes, just `go build` for any modifications. 

## Installation notes

Before getting surprised like I did, note that the `home.html` is hosted by the webserver that launches from the go 
binary. This choice was made for the most simplicity. If you wish to investigate a decoupled websocket server then 
there may need to be some investigation in the source code.
